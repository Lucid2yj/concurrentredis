# concurrentRedis

레디스에서 동시성 제어를 위한 처리 예제


## Description
멀티서버로 구성된 환경에서 레디스를 사용하다보면 동시 접근에 대한 처리(Concurrency)가 필요한 경우가 있습니다.
동시성 처리에 익숙한 분들은 task 단위의 쓰레드 처리방식이나 각 언어에서 제공하는 기능을 잘 사용하실거라 고민할 필요가 없습니다.

이 예제에서는 단순히 레디스의 Single-thread를 의존하여 Lua script를 활용한 예제와 Java의 ConcurrentHashMap을 이용한 레디스 동시성 처리를 구현하였으며 결론적으로 레디스 Single-thread 의존적 방식은 매우 비효율적임을 시사합니다.


## 필수 요소(Prerequisites)
레디스 서버 ( 2.6.0 이상 )

## 테스트 방식
던전 정보를 만들어 플레이어 수와 클리어 수를 각 쓰레드들이 동시적으로 갱신 후 레디스 저장.

최종 던전 정보는 Player 500, Clear 500 회.

- 1번 던전은 레디스 Single-thread 의존적 방식 : [RedisManager][RedisManager]
- 2번 던전은 Java의 ConcurrentHashMap을 이용한 방식 : [MapManager][MapManager]

[RedisManager]: https://gitlab.com/Lucid2yj/concurrentredis/-/blob/main/src/main/java/concurrentRedis/RedisManager.java
[MapManager]: https://gitlab.com/Lucid2yj/concurrentredis/-/blob/main/src/main/java/concurrentRedis/MapManager.java

## 결과
- 1번 던전은 평균적으로 10~30개의 동기화 실패를 보입니다.
- 2번 던전은 정확히 Player 500, Clear 500 의 데이터를 보유합니다.

### 결론
호기심에서 시작된 레디스 Single-thread 의존적 방식은 예상과 달리 적은 쓰레드 수의 경합에도 취약한 모습을 보여주었습니다. Java의 ConcurrentHashMap 이 매우 잘 만들어진 기능이기는 하나 멀티서버 환경에서는 전용 서버를 구축해야하고 사용률에 따라 샤딩을 해야하기 때문에 작은 단위의 데이터를 이용하기에는 부담스러운 환경이기는 합니다. 또 메모리 관리를 주기적으로 해주는 스케줄러까지 필요하기 때문에 항상 Java의 ConcurrentHashMap를 이용한 동시성 처리가 옳은 선택이라 결정하긴 힘들 것입니다.

