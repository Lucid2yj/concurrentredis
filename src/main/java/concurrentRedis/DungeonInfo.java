package concurrentRedis;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

@Getter
public class DungeonInfo {
	public static final int UPDATE_TYPE_NONE = 0;
	public static final int UPDATE_TYPE_PLAYER = 1;
	public static final int UPDATE_TYPE_CLEAR = 2;
	
	/** 레디스 Concurrncy 버전. 
	 *  - set Count를 버전값으로 사용 
	 */
	@Setter private Integer redisVersion = 0;
	
	
	/** 던전 정보 */
	private Integer dungeonNo = 0;
	private Integer playerCnt = 0;
	private Integer clearCnt = 0;
	
	public DungeonInfo(int no) {
		dungeonNo = no;
	}
	
	
	
	/** 
	 * 던전 정보 갱신 로직
	 * - 레디스 동기화 필요 시 데이터 동기화 후 아래 로직을 재수행 하여 갱신  
	 */
	@Getter @Setter
	public static class UpdateData {
		private Integer updateType = UPDATE_TYPE_NONE;
		private Integer addCnt = 0;
	}
	public boolean update(UpdateData _data) {
		switch(_data.getUpdateType()) {
			case UPDATE_TYPE_PLAYER:
				playerCnt += _data.getAddCnt();
				break;
			case UPDATE_TYPE_CLEAR:
				clearCnt += _data.getAddCnt();
				break;
			default:
				return false;
		}
		return true;
	}
	
	
	/**
	 * 던전 정보를 Json으로 변환
	 * @return
	 */
	public String toJson() {
		JSONObject dJson = new JSONObject ();
		dJson.put("dungeonNo", dungeonNo);
		dJson.put("playerCnt", playerCnt);
		dJson.put("clearCnt", clearCnt);
		
		return dJson.toString();
	}
	
	/**
	 * Json을 던전 정보로 변환
	 * @param dJsonString
	 * @return
	 */
	public boolean parseJson(String dJsonString) {
		try {
			JSONObject dJson = new JSONObject (dJsonString);
			dungeonNo = dJson.getInt("dungeonNo");
			playerCnt = dJson.getInt("playerCnt");
			clearCnt = dJson.getInt("clearCnt");
		}
		catch (JSONException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
}
