package concurrentRedis;

import java.util.concurrent.ConcurrentHashMap;

import concurrentRedis.DungeonInfo.UpdateData;

public class MapManager {
	private static final MapManager __instance = new MapManager();
	public static MapManager getInstance() {
		return __instance;
	}
	
	private ConcurrentHashMap<Integer, DungeonInfo> __dungeonList = new ConcurrentHashMap<Integer, DungeonInfo>();
	
	public void updateDungeonInfo(int _dNo, UpdateData _updateData) {
		
		__dungeonList.compute(_dNo, (dNo, dungeonInfo)->{
			// 던전이 없을 경우 새로 로드
			if (dungeonInfo == null) {
				dungeonInfo = RedisManager.getInstance().getDungeonInfoByGet(_dNo);
				// 레디스에도 없을 경우 새로 생성
				if (dungeonInfo == null) {
					System.out.println(String.format("[ConcurrentHashMap][updateDungeonInfo] Not found dungeon info in redis. no %d", _dNo));
					dungeonInfo = new DungeonInfo(_dNo);
				}
			}
			
			// 던전 업데이트
			dungeonInfo.update(_updateData);
			
			// 던전 저장
			RedisManager.getInstance().setDungeonInfoBySet(dungeonInfo);
			return dungeonInfo;
		});
	}
}
