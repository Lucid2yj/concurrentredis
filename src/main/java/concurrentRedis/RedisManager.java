package concurrentRedis;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import concurrentRedis.DungeonInfo.UpdateData;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisNoScriptException;

public class RedisManager {
	private static final RedisManager __instance = new RedisManager();
	public static RedisManager getInstance() {
		return __instance;
	}
	
	public JedisPool __pool = null;
	
	// Lua script sha
	private String __shaGetData;
	private String __shaSetData;
	private Integer __failCnt = 0;
	
	public RedisManager() {
		init();
		loadScript();
	}
	
	public void destroy() {
		__pool.destroy();
	}
	
	/**
	 * 레디스 풀 생성
	 */
	void init() {
		int maxActive = 4000;
        int maxIdle = 500;
        int maxWait = 6000;
        int minIdle = 100;

        JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(maxActive);
		config.setMaxIdle(maxIdle);
		config.setMinIdle(minIdle);
		config.setMaxWaitMillis(maxWait);

		String ip = "127.0.0.1";
		int port = 6379;
		int redisTimeout = 6000;
		__pool = new JedisPool(config, ip, port, redisTimeout);
		
		Jedis resource = null;
		try {
			resource = __pool.getResource();
			resource.getClient().getSocket().setTcpNoDelay(true);
		} catch (SocketException e) {
			e.printStackTrace();
			if ( null != resource) resource.close();
		}
		
		System.out.println(String.format("[RedisManager][init] Connect redis. ip %s port %d", ip, port));
	}
	
	/**
	 * 루아 스크립트 sha 생성
	 */
	private void loadScript() {
		loadScriptShaGetData();
		loadScriptShaSetData();
	}
	
	
	/**
	 * 데이터 가져오기 스크립트 Sha 생성
	 */
	private void loadScriptShaGetData() {
		// KEYS[1] : version key
		// KEYS[2] : dungeon key
        // result[0] : version
        // result[1] : dungeon json string
		
		StringBuilder script = new StringBuilder();
		script.append("local curVersion = tonumber(redis.call('get', KEYS[1])) ");
		script.append("if curVersion == nil then ");
		script.append("curVersion = 0 ");
		script.append("redis.call('set', KEYS[1], curVersion) ");
		script.append("end ");
		script.append("local result = {} ");
		script.append("result[1] = curVersion ");
		script.append("result[2] = redis.call('get', KEYS[2]) ");
		script.append("return result ");
		
		Jedis resource = null;
		try {
			resource = __pool.getResource();
			__shaGetData = resource.scriptLoad(script.toString());
		} 
		// 커넥션 풀 오류
        catch (JedisConnectionException jce) {
        	System.out.println(String.format("[RedisManager][loadScriptShaGetData] Fail to get resource pool."));
        	if ( null != resource) resource.close();

        }
		catch (Exception e) {
            e.printStackTrace();
            if ( null != resource) resource.close();
        }
	}
	
	
	/**
	 * 데이터 가져오기 스크립트 Sha 생성
	 */
	private void loadScriptShaSetData() {
		// KEYS[1] : version key
		// KEYS[2] : dungeon key
        // ARGV[1] : prev version
        // ARGV[2] : new version
        // ARGV[3] : dungeon data
        // ARGV[4] : ttl
		
		StringBuilder script = new StringBuilder();
		script.append("local curVersion = tonumber(redis.call('get', KEYS[1])) ");
		script.append("local prevVersion = tonumber(ARGV[1]) ");
		script.append("if curVersion == nil then  ");
		script.append("curVersion = prevVersion ");
		script.append("elseif curVersion ~= prevVersion then ");
		script.append("return -1 ");
		script.append("end ");
		script.append("redis.call('setex', KEYS[1], ARGV[4], ARGV[2]) ");
		script.append("redis.call('setex', KEYS[2], ARGV[4], ARGV[3]) ");
		script.append("return 0 ");
		
		Jedis resource = null;
		try {
			resource = __pool.getResource();
			__shaSetData = resource.scriptLoad(script.toString());
		} 
		// 커넥션 풀 오류
        catch (JedisConnectionException jce) {
        	System.out.println(String.format("[RedisManager][loadScriptShaSetData] Fail to get resource pool."));
        	if ( null != resource) resource.close();

        }
		catch (Exception e) {
            e.printStackTrace();
            if ( null != resource) resource.close();
        }
	}
	
	
	public String getKeyDungeonInfo(int _dNo) {
		return String.format("dInfo:%d", _dNo);
	}
	public String getKeyDungeonVersion(int _dNo) {
		return String.format("dVer:%d", _dNo);
	}
	
	/**
	 * 던전 정보 가져오기
	 * @param dHash
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DungeonInfo getDungeonInfoByScript(int _dNo) {
		
		DungeonInfo dungeonInfo = null;
		
		String dVerKey = getKeyDungeonVersion(_dNo);
		String dInfoKey = getKeyDungeonInfo(_dNo);
		
		// KEYS[1] : version key
		// KEYS[2] : dungeon key
        // result[0] : version
        // result[1] : dungeon json string
		
		// Key
        List<byte[]> keys = new ArrayList<byte[]>();
        keys.add(dVerKey.getBytes());
        keys.add(dInfoKey.getBytes());

        // ARGV
        List<byte[]> args = new ArrayList<byte[]>();
		
	    // sha 키를 이용해 레디스에 캐싱된 스크립트를 실행
        ArrayList<byte[]> result = new ArrayList<byte[]>();
        
        int retryCnt = 0;
		Jedis resource = null;
		while(retryCnt < 10) {
			try {
	        	// 스크립트 실행
				resource = __pool.getResource();
	        	result = (ArrayList<byte[]>) resource.evalsha(__shaGetData.getBytes(), keys, args);
	        	
	        	// 데이터 로드
	    		dungeonInfo = new DungeonInfo(_dNo);
	    		if (dungeonInfo.parseJson(new String(result.get(1))) == false) {
	    			System.out.println(String.format("[RedisManager][getDungeonInfoByScript] Fail to ParseJSON. dVerKey %s dInfoKey %s jsonString %s", dVerKey, dInfoKey, result.get(1)));
	    			dungeonInfo = null;
	    			break;
	    		}
	    		
	    		int version = Integer.parseInt(String.valueOf(result.get(0)));
	    		dungeonInfo.setRedisVersion(version);
	    		break;
	        } 
			catch (JedisNoScriptException jscriptE) {
	        	System.out.println(String.format("[RedisManager][getDungeonInfoByScript] Retry load script. sha %s dVerKey %s dInfoKey %s", __shaGetData, dVerKey, dInfoKey));
	        	
	        	// 스크립트 로드 후 재시도. 재귀는 무한호출에 빠질 수 있으므로 사용 X.
	        	loadScriptShaGetData();
	        	if (resource != null) resource.close();
	    	} 
			catch(JedisConnectionException e) {
				e.printStackTrace();
				if (resource != null) resource.close();
			}
			catch (Exception e) {
	    		e.printStackTrace();
	    		if (resource != null) resource.close();
	    		break;
	    	}
			retryCnt++;
		}
		
		// 10회동안 리소스 획득에 실패
		if (resource == null && retryCnt >= 10) {
			System.out.println(String.format("[RedisManager][getDungeonInfoByGet] Fail to get redis pool. retryCnt %d", retryCnt));
		}
        
        if (resource != null) resource.close();
		
		
		return dungeonInfo;
		
	}
	
	
	/**
	 * 레디스에 던전 정보 저장
	 * @param _dNo
	 * @param _dungeonInfo
	 * @param _updateData
	 * @return
	 */
	public void setDungeonInfoByScript(int _dNo, DungeonInfo _dungeonInfo, UpdateData _updateData) {
		String dVerKey = getKeyDungeonVersion(_dNo);
		String dInfoKey = getKeyDungeonInfo(_dNo);
		
		// KEYS[1] : version key
		// KEYS[2] : dungeon key
        // ARGV[1] : prev version
        // ARGV[2] : new version
        // ARGV[3] : dungeon data
        // ARGV[4] : ttl
		// result 0 : 저장 성공
		// result -1 : 갱신 필요
		
		// Key
        List<byte[]> keys = new ArrayList<byte[]>();
        keys.add(dVerKey.getBytes());
        keys.add(dInfoKey.getBytes());
		
        
	    // sha 키를 이용해 레디스에 캐싱된 스크립트를 실행
        Long result = -1l;
        int retryCnt = 0;
        Jedis resource = null;
        DungeonInfo prevDungeonInfo = _dungeonInfo;
        while(result != 0 && retryCnt < 10) {
        	
            // ARGV
            int prevVer = _dungeonInfo.getRedisVersion().intValue();
            List<byte[]> args = new ArrayList<byte[]>();
            args.add(Integer.toString(prevVer).getBytes());
            args.add(Integer.toString(prevVer+1).getBytes());
            args.add(_dungeonInfo.toJson().getBytes());
            args.add(Integer.toString(86400).getBytes());	// ttl 24시간
            
        	try {
        		// 스크립트 실행
            	resource = __pool.getResource();            	
            	result = (Long) resource.evalsha(__shaSetData.getBytes(), keys, args);
            	
            	// 레디스 데이터 동기화 필요 시 새로 가져온 뒤 update 재실행
            	if (result.equals(-1l)) {
//            		System.out.println(String.format("[RedisManager][setDungeonInfoByScript] Need update."));
            		_dungeonInfo = getDungeonInfoByScript(_dNo);
            		_dungeonInfo.update(_updateData);
            		retryCnt++;
            		continue;
            	}
            	// 저장에 성공했지만 동기화가 돌았으면 새로운 정보로 재구성
            	else if(result.equals(0l) && retryCnt > 0) {
            		prevDungeonInfo.parseJson(_dungeonInfo.toJson());
            	}
            	break;
            } 
        	catch (JedisNoScriptException jscriptE) {
            	System.out.println(String.format("[RedisManager][setDungeonInfoByScript] retry load script. sha %s dVerKey %s dInfoKey %s", __shaSetData, dVerKey, dInfoKey));
            	
            	// 스크립트 로드 후 재시도. 재귀는 무한호출에 빠질 수 있으므로 사용 X.
            	loadScriptShaSetData();
        		if (resource != null) resource.close();
        	} 
        	catch(JedisConnectionException e) {
				e.printStackTrace();
        		if (resource != null) resource.close();
			}
        	catch (Exception e) {
        		System.out.println(String.format("[RedisManager][setDungeonInfoByScript] failed to excute script. sha %s dVerKey %s dInfoKey %s", __shaSetData, dVerKey, dInfoKey));
        		e.printStackTrace();
        		if (resource != null) resource.close();
        		break;
        	}
        	
        	retryCnt++;
        }
        
        // 실패한 로그
        if (result != 0) {
        	__failCnt ++;
        	System.out.println(String.format("[RedisManager][setDungeonInfoByScript] Fail to save dungeon. failCnt %d result %d retryCnt %d", __failCnt, result, retryCnt));
        }
        
		return;
	}
	
	
	/**
	 * get을 이용한 데이터 가져오기
	 * @param _dNo
	 * @return
	 */
	public DungeonInfo getDungeonInfoByGet(int _dNo) {
		String dInfoKey = getKeyDungeonInfo(_dNo);
		DungeonInfo dungeonInfo = null;
		int retryCnt = 0;
		Jedis resource = null;
		while(retryCnt < 10) {
			try {

				// 데이터 가져오기
				resource = __pool.getResource();
				String dData = resource.get(dInfoKey);
				if (dData == null) {
					System.out.println(String.format("[RedisManager][getDungeonInfoByGet] Not found data. dNo %d", _dNo));
					break;
				}
				else {
					dungeonInfo = new DungeonInfo(_dNo);
					if (dungeonInfo.parseJson(dData) == false) {
						System.out.println(String.format("[RedisManager][getDungeonInfoByGet] Fail to parse json. dNo %d data %s", _dNo, dData));
						dungeonInfo = null;
						break;
					}
				}
			}
			catch(JedisConnectionException e) {
				e.printStackTrace();
				if (resource != null) resource.close();
			}
			catch(Exception e) {
				e.printStackTrace();
				if (resource != null) resource.close();
				break;
			}
			retryCnt++;
		}
		
		// 10회동안 리소스 획득에 실패
		if (resource == null && retryCnt >= 10) {
			System.out.println(String.format("[RedisManager][getDungeonInfoByGet] Fail to get redis pool. retryCnt %d", retryCnt));
		}
		
		if (resource != null) resource.close();
		return dungeonInfo;
	}
	
	/**
	 * set을 이용한 데이터 저장
	 * @param _dungeonInfo
	 */
	public void setDungeonInfoBySet(DungeonInfo _dungeonInfo) {
		String dInfoKey = getKeyDungeonInfo(_dungeonInfo.getDungeonNo());
		int retryCnt = 0;
		Jedis resource = null;
		while(retryCnt < 10) {
			try {
				// 데이터 저장
				resource = __pool.getResource();
				resource.set(dInfoKey, _dungeonInfo.toJson());
				break;
			}
			catch(JedisConnectionException e) {
				e.printStackTrace();
				if (resource != null) resource.close();
			}
			retryCnt ++;
		}
		
		// 10회동안 리소스 획득에 실패
		if (resource == null && retryCnt >= 10) {
			System.out.println(String.format("[RedisManager][setDungeonInfoBySet] Fail to get redis pool. retryCnt %d", retryCnt));
		}
		
		if (resource != null) resource.close();
		return ;
	}
}
