package concurrentRedis;

import concurrentRedis.DungeonInfo.UpdateData;

public class ConcurrentTest {
	public static void main(String[] args) {
		RedisManager.getInstance();
		
		// 던전 정보 생성 후 첫 저장
		int dNo = 1;
		final DungeonInfo dInfo1 = new DungeonInfo(dNo);
		UpdateData _updateData = new UpdateData();
		RedisManager.getInstance().setDungeonInfoByScript(dNo, dInfo1, _updateData);
		
		dNo = 2;
		final DungeonInfo dInfo2 = new DungeonInfo(dNo);
		MapManager.getInstance().updateDungeonInfo(dInfo2.getDungeonNo(), _updateData);
		
		
		// 5개의 플래이어 카운트와 클리어 카운트를 증가시키는 쓰레드 실행 
		for (int i=0; i<5; i++) {
			
			// 레디스 쓰레드
			new Thread()
			{
				@Override
				public void run()
				{
					excuteOnRedis(dInfo1, DungeonInfo.UPDATE_TYPE_PLAYER);
				}

			}.start(); 
			new Thread()
			{
				@Override
				public void run()
				{
					excuteOnRedis(dInfo1, DungeonInfo.UPDATE_TYPE_CLEAR);
				}

			}.start(); // 스레드 실행
			
			
			// 맵 쓰레드
			new Thread()
			{
				@Override
				public void run()
				{
					excuteOnMap(dInfo2, DungeonInfo.UPDATE_TYPE_PLAYER);
				}

			}.start(); // 스레드 실행
			new Thread()
			{
				@Override
				public void run()
				{
					excuteOnMap(dInfo2, DungeonInfo.UPDATE_TYPE_CLEAR);
				}

			}.start(); // 스레드 실행
		}
	}
	
	
	/**
	 * 레디스를 이용한 업데이트
	 * @param _dInfo
	 * @param _updateType
	 */
	private static void excuteOnRedis(DungeonInfo _dInfo, int _updateType) {
		
		UpdateData data = new UpdateData();
		data.setUpdateType(_updateType);
		data.setAddCnt(1);
		
		for(int i=0; i<10 ; i++){
			try {
				Thread.sleep(1000);
				
				for (int j=0; j<10; j++) {
					RedisManager.getInstance().setDungeonInfoByScript(_dInfo.getDungeonNo(), _dInfo, data);
				}
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 맵을 이용한 데이터 업데이트
	 * @param _dInfo
	 * @param _updateType
	 */
	private static void excuteOnMap(DungeonInfo _dInfo, int _updateType) {
		UpdateData data = new UpdateData();
		data.setUpdateType(_updateType);
		data.setAddCnt(1);
		
		for(int i=0; i<10 ; i++){
			try {
				Thread.sleep(1000);
				
				for (int j=0; j<10; j++) {
					MapManager.getInstance().updateDungeonInfo(_dInfo.getDungeonNo(), data);
				}
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
